------------------------------- Challenge instructions -------------------------------

Once you complete this exercise, please email the submission to jobs@theta-llc.com with your name, a hyphen, the position you're interviewing for, a colon, and 'Interview Challenge Submission' for in the subject line. (e.g. 'John/Jane Public - DevOps Engineer: Interview Challenge Submission').

Please don't hesitate to contact your interviewer if you have any questions or feedback about the test. We're looking for cleverness and attention to detail, not how quickly you can get it done.

Please use whichever language you're comfortable with (unless the position you applied to had a specfic language/technology specification). As a System Admin, DevOps Engineer, SRE, or SysOps Engineer, you're expected to support, administer, and maintain a ton of different applications running on different frameworks and written in different languages, running on different OS's; so represent yourself with your strongest scripting language/open-source tool choice. As long as you can provide instructions on how to execute it so we can see the results, it's fine with us.

**Project:** Web Application Monitoring & Alerting

**Estimated Time to Complete:** 3-5 hours.

**Description:** Given a dummy web application, configure a monitoring system and an alerting system.

**Required Features:**
- Monitor server metrics (CPU, memory, disk usage).
- Monitor application-specific metrics (response time, error rates).
- Configure alerts for abnormal behavior.
- Create a dashboard to visualize metrics.

**Evaluation Criteria:**
- Monitoring coverage (are all critical aspects monitored?).
- Alert configuration (are alerts meaningful and not too noisy?).
- Documentation on setup and any decisions made.
- Choice of monitoring tools.

**Mock Data:**
Refer to the `system_metrics_mock_data.json` for sample metrics related to the dummy application.

**Submission Requirements:**
- Zip up any configuration files or scripts and submit following the challenge instructions above.
- Provide screenshots or an access link (if available) to your dashboard of metrics.

**FAQS:**

**Q:** What will you be grading me on?

**A:** See the above "Evaluation Criteria"

**Q:** Will I have a chance to explain my choices?

**A:** Feel free to comment your code, or put explanations in a pull request within the repo. If we proceed to a phone interview, we’ll be asking questions about why you made the choices you made in the technical interview.

**Q:** Why doesn't the test include XY and Z?

**A:** Dope question! PLEASE free to tell us how to make the test better. Or, you know, fork it and improve it! 
