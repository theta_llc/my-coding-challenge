------------------------------- Challenge instructions -------------------------------

Once you complete this exercise, please email the submission to jobs@theta-llc.com with your name, a hyphen, the position you're interviewing for, a colon, and 'Interview Challenge Submission' for in the subject line. (e.g. 'John/Jane Public - Frontend Developer: Interview Challenge Submission').

Please don't hesitate to contact your interviewer if you have any questions or feedback about the test. We're looking for cleverness and attention to detail, not how quickly you can get it done.

Please use whichever language you're comfortable with (unless the position you applied to had a specfic language/technology specification). As a developer, you're expected to have worked with a plethora of different programming languages, so represent yourself with your strongest language choice. As long as you can provide instructions on how to execute it so we can see the results, it's fine with us.

**Project:** Weather Application Responsive Dashboard

**Estimated Time to Complete:** 4-6 hours.

**Description:** Given mock API data (or a real one like OpenWeatherMap), design and develop a dashboard that displays current weather, forecasts, and historical data.

**Required Features:**
- Search functionality for different locations.
- Display current weather conditions (temperature, humidity, wind speed, etc.).
- Visualize forecast for the next 7 days.
- A graphical representation (like a chart) for historical weather data.
- Mobile-responsive design.

**Evaluation Criteria:**
- User interface and user experience.
- Responsiveness across devices.
- Code organization and modularity.
- Error handling (e.g., for failed API requests).

**Mock Data:**
Refer to the `weather_mock_data.json` for sample weather data from an API if not using a real one.

**Submission Requirements:**
- Zip up all code and submit following the challenge instructions above.
- Ensure you include any necessary setup instructions or documentation in your submission.

**FAQS:**

**Q:** What will you be grading me on?

**A:** See the above "Evaluation Criteria"

**Q:** Will I have a chance to explain my choices?

**A:** Feel free to comment your code, or put explanations in a pull request within the repo. If we proceed to a phone interview, we’ll be asking questions about why you made the choices you made in the technical interview.

**Q:** Why doesn't the test include XY and Z?

**A:** Dope question! PLEASE free to tell us how to make the test better. Or, you know, fork it and improve it! 
