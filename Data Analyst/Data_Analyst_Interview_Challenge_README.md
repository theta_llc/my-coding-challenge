------------------------------- Challenge instructions -------------------------------

Once you complete this exercise, please email the submission to jobs@theta-llc.com with your name, a hyphen, the position you're interviewing for, a colon, and 'Interview Challenge Submission' for in the subject line. (e.g. 'John/Jane Public - Data Analyst: Interview Challenge Submission').

Please don't hesitate to contact your interviewer if you have any questions or feedback about the test. We're looking for cleverness and attention to detail, not how quickly you can get it done.

Please use whichever language you're comfortable with (unless the position you applied to had a specfic language/technology specification). As a data analyst, you're expected to have worked with a plethora of different types of data, so represent yourself with your strongest technocal choice. As long as you can provide instructions on how to follow along so we can see the results, it's fine with us.

**Project:** HealthData Insights Platform

**Estimated Time to Complete:** 6-10 hours.

**Description:** Our firm has been hired to provide data analyst support for a HealthData Insights Platform that was developed for a state government's health department. As the data analyst on our team, you have been tasked to analyze, interpret, and present data encountered in the HealthData Insights platform. The data you have been asked to focus on handling comes from electronic lab data (ELR) related to reportable health conditions such as COVID-19, HIV, and lupus.

**Task Overview:**
You are provided with a dataset that simulates ELR data from multiple healthcare providers. This data includes patient demographics, test results, and dates of service. Your task is to analyze this data to identify trends, anomalies, and provide insights that could help improve public health reporting and response strategies.

**Dataset Description:**

- Patient_ID: Unique identifier for patients
- Age: Age of the patients
- Gender: Gender of the patients
- Test_Type: Type of test performed (e.g., COVID-19, HIV)
- Test_Result: Result of the test (Positive, Negative)
- Date_of_Test: Date when the test was performed
- Provider_ID: Identifier for the healthcare provider

**Instructions:**

**1. Data Cleaning and Preparation:**

- Clean the data to handle any inconsistencies or missing values.
- Prepare the data for analysis, ensuring it is in a suitable format for the tasks below.

**2. Data Analysis:**

- Calculate the prevalence of each condition within the dataset.
- Identify any significant trends in test results over time.
- Analyze the data for any demographic patterns in test outcomes.

**3. Data Visualization:**

- Create visualizations to represent your findings from the data analysis:
    - A bar chart showing the prevalence of each condition.
    - A line graph depicting trends over time.
    - Pie charts or similar visualizations highlighting demographic patterns.

**4. Insights and Recommendations:**

- Based on your analysis, provide insights into the data. For example, are there any surprising trends or patterns?
- Suggest potential public health actions or further areas of study based on your findings.

**5. Report:**

- Compile your findings, visualizations, insights, and recommendations into a concise report. Your report should be clear enough to be understood by non-technical stakeholders.

**Evaluation Criteria:**
- Accuracy of data cleaning and preparation.
- Effectiveness and clarity of data analysis and visualizations.
- Relevance and depth of insights and recommendations.
- Quality of communication in the report, particularly the ability to convey complex information in an accessible manner.

**Dataset Access from HealthData Insights Platform:**

Refer to the `HealthData_Insights_Full_Dataset.csv` and `HealthData_Insights_Anomalies_Dataset.csv` for sample data related to the HealthData Insights Platform.

Ensure you handle all data in accordance with standard data privacy practices. Refer to `Data Dictionary for HealthData Insights Dataset.md` for detailed information on each dataset field.

**Submission Requirements:**
- Complete your analysis and compile your findings into a PDF document.

**FAQS:**

**Q:** What will you be grading me on?

**A:** See the above "Evaluation Criteria"

**Q:** Will I have a chance to explain my choices?

**A:** Feel free to comment your code, or put explanations in a pull request within the repo. If we proceed to a phone interview, we’ll be asking questions about why you made the choices you made in the technical interview.

**Q:** Why doesn't the test include XY and Z?

**A:** Dope question! PLEASE free to tell us how to make the test better. Or, you know, fork it and improve it! 
