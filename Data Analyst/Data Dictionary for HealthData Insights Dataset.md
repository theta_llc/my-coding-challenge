### **Data Dictionary for HealthData Insights Dataset**

Below is the data dictionary for the HealthData Insights dataset, which describes each field in the dataset and provides information on the data characteristics.

---

#### **Data Dictionary**

| Column Name | Description | Data Type | Notes |
| ----- | ----- | ----- | ----- |
| Patient\_ID | Unique identifier for each patient | String | Format: 'PAT\#\#\#\#' (e.g., PAT0001) |
| Age | Age of the patient | Integer | Values range from 1 to 100; some anomalies |
| Gender | Gender of the patient | String | Possible values: 'Male', 'Female', 'Non-binary' |
| Test\_Type | Type of test performed | String | Possible values: 'COVID', 'HIV', 'Lupus' |
| Test\_Result | Result of the test | String | Possible values: 'Positive', 'Negative'; some missing values |
| Date\_of\_Test | Date when the test was performed | DateTime | Format: YYYY-MM-DD |
| Provider\_ID | Identifier for the healthcare provider | String | Format: 'PROV\#\#\#' (e.g., PROV001) |

---

#### **Field Descriptions**

1. **Patient\_ID**:  
   * **Description**: A unique identifier assigned to each patient in the dataset.  
   * **Format**: A string prefixed with 'PAT' followed by a four-digit number (e.g., PAT0001).  
2. **Age**:  
   * **Description**: Represents the age of the patient.  
   * **Data Characteristics**:  
     * Typical values range from 1 to 100\.  
     * Some values may be illogical (e.g., \-1, 0, 105\) as intentional anomalies.  
3. **Gender**:  
   * **Description**: The gender of the patient.  
   * **Possible Values**: 'Male', 'Female', 'Non-binary'.  
4. **Test\_Type**:  
   * **Description**: Indicates the type of medical test performed.  
   * **Possible Values**:  
     * 'COVID': COVID-19 test.  
     * 'HIV': HIV test.  
     * 'Lupus': Lupus test.  
5. **Test\_Result**:  
   * **Description**: The result of the medical test.  
   * **Possible Values**:  
     * 'Positive': Indicates a positive test result.  
     * 'Negative': Indicates a negative test result.  
     * **Data Characteristics**: Some values may be missing.  
6. **Date\_of\_Test**:  
   * **Description**: The date on which the test was conducted.  
   * **Format**: Date in YYYY-MM-DD format.  
7. **Provider\_ID**:  
   * **Description**: A unique identifier assigned to the healthcare provider.  
   * **Format**: A string prefixed with 'PROV' followed by a three-digit number (e.g., PROV001).

---

#### **Additional Notes**

* **Data Anomalies**: The dataset includes intentional anomalies to test data cleaning skills, such as illogical age values and missing test results.  
* **Privacy**: All data is synthetic and anonymized to ensure privacy and compliance with data protection standards.

