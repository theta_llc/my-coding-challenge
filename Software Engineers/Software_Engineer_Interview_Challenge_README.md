------------------------------- Challenge instructions -------------------------------

Once you complete this exercise, please email the submission to jobs@theta-llc.com with your name, a hyphen, the position you're interviewing for, a colon, and 'Interview Challenge Submission' for in the subject line. (e.g. 'John/Jane Public - Back-End Developer: Interview Challenge Submission').

Please don't hesitate to contact your interviewer if you have any questions or feedback about the test. We're looking for cleverness and attention to detail, not how quickly you can get it done.

Please use whichever language you're comfortable with (unless the position you applied to had a specfic language/technology specification). As a software engineer, you're expected to have worked with a plethora of different programming languages, so represent yourself with your strongest language choice. As long as you can provide instructions on how to execute it so we can see the results, it's fine with us.

**Project:** Bookstore Application

**Estimated Time to Complete:** 4-6 hours.

**Description:** Build a CRUD app for a bookstore. The application should be able to add books, view a list of books, update book details, and delete books.

**Required Features:**
- Authentication system.
- Ability to categorize books.
- Search functionality.
- API endpoints for all CRUD operations.
- Basic UI to interact with the application.

**Evaluation Criteria:**
- Code quality and organization.
- Database schema design.
- Proper error handling.
- Security considerations (e.g., how passwords are stored, SQL injection prevention).

**Mock Data:**
Refer to the `books_mock_data.json` for sample data for your application.

**Submission Requirements:**
After development please: 
- Zip up your code and follow the challenge instructions above.
- Provide instructions on how to execute your solutions

**FAQS:**

**Q:** What will you be grading me on?

**A:** See the above "Evaluation Criteria"

**Q:** Will I have a chance to explain my choices?

**A:** Feel free to comment your code, or put explanations in a pull request within the repo. If we proceed to a phone interview, we’ll be asking questions about why you made the choices you made in the technical interview.

**Q:** Why doesn't the test include XY and Z?

**A:** Dope question! PLEASE free to tell us how to make the test better. Or, you know, fork it and improve it! 
