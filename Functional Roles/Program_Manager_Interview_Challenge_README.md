------------------------------- Challenge instructions -------------------------------

Once you complete this exercise, please email the submission to jobs@theta-llc.com with your name, a hyphen, the position you're interviewing for, a colon, and 'Interview Challenge Submission' for in the subject line. (e.g. 'John/Jane Public - Program Manager: Interview Challenge Submission').

Please don't hesitate to contact your interviewer if you have any questions or feedback about the test. We're looking for cleverness and attention to detail, not how quickly you can get it done.

Please use whichever tools you're comfortable with (unless the position you applied to had a specfic tool/technology specification). As a Program Manager, you're expected to oversee the fulfillment of larger organizational goals. You coordinate activities between multiple projects without directly managing them. Instead, you manage the main program, giving detailed attention to program strategy, project delegation, and program implementation, so represent yourself with your strongest tool choices that demonstrate your methods and approaches. As long as you can provide reasoning on your choices, it's fine with us.

**Project:** Digital Renaissance 2035

**Estimated Time to Complete:** 4-5 hours.

**Scenario:** theta. is leading a program titled "Digital Renaissance 2035" for a state government. The vision is to not only digitize public services but also promote tech startups, improve statewide internet infrastructure, and introduce digital education programs in schools. Multiple agencies, from the education department to commerce, are stakeholders.

**Task:**

1. **Program Backlog Creation**:
   - Identify key projects under this multi-faceted program.
   - For each project, define the MVP and create a high-level backlog.
   - **Constraint**: Prioritize and propose a maximum of 5 key projects under the program, keeping in mind the diverse stakeholders.
2. **Resource Allocation & Cross-collaboration**:
   - Allocate resources for the projects.
   - Detail strategies for inter-departmental collaboration and synchronization.
   - **Constraint**: You have a team of 4 UI/UX designers, 5 frontend developers, 5 backend developers, 2 cloud specialists, 2 cybersecurity experts, and 3 digital marketing specialists.
3. **KPIs & Metrics**:
   - Define KPIs that capture the essence of the program's success.
   - **Constraint**: Limit your KPIs to 7, ensuring they cover both digital transformation and societal impact.

**Submission Requirements:**
- Compile your backlogs, resource plans, and KPIs into a comprehensive document and submit following the challenge instructions above.

**Evaluation Criteria:**

1. **Program Backlog Creation**:
   - Ability to identify and prioritize key projects aligned with the program's vision.
   - Quality of the MVP definition for each project.
   - Consideration of diverse stakeholders and their interests.
2. **Resource Allocation & Cross-collaboration**:
   - Effective and logical resource allocation across projects.
   - Strategies for fostering collaboration among different teams and departments.
   - Understanding of the broader objectives and how individual projects contribute.
3. **KPIs & Metrics**:
   - Relevance and clarity of chosen KPIs.
   - Balance between quantitative and qualitative KPIs.
   - Alignment of KPIs with both digital transformation and societal impact objectives.

**Additional Criteria**:
- **Presentation & Structure**: Clear and organized presentation of ideas, logical flow, and coherence.
- **Justification & Rationale**: Ability to provide reasons for decisions, showing depth of thought.
- **Adherence to Constraints**: Respecting the given constraints while showcasing flexibility and creativity.
- **Real-world Relevance**: Demonstrating an understanding of real-world challenges, user needs, and stakeholder interests.
- **Communication Skills**: Clarity in expression, ability to convey complex ideas simply, and attention to detail.

**FAQS:**

**Q:** What will you be grading me on?

**A:** See the above "Evaluation Criteria"

**Q:** Will I have a chance to explain my choices?

**A:** Feel free to comment your code, or put explanations in a pull request within the repo. If we proceed to a phone interview, we’ll be asking questions about why you made the choices you made in the technical interview.

**Q:** Why doesn't the test include XY and Z?

**A:** Dope question! PLEASE free to tell us how to make the test better. Or, you know, fork it and improve it! 
