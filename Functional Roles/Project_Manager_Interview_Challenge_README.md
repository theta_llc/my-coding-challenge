------------------------------- Challenge instructions -------------------------------

Once you complete this exercise, please email the submission to jobs@theta-llc.com with your name, a hyphen, the position you're interviewing for, a colon, and 'Interview Challenge Submission' for in the subject line. (e.g. 'John/Jane Public - Project Manager: Interview Challenge Submission').

Please don't hesitate to contact your interviewer if you have any questions or feedback about the test. We're looking for cleverness and attention to detail, not how quickly you can get it done.

Please use whichever tools you're comfortable with (unless the position you applied to had a specfic tool/technology specification). As a Project Manager you organize, plan, and execute projects while working within restraints like budgets and schedules. You lead entire scrum teams, define project goals, communicate with stakeholders, and see a project through to its closure, so represent yourself with your strongest tool choices that demonstrate your methods and approaches. As long as you can provide reasoning on your choices, it's fine with us.

**Project:** Climate-Emergency Disaster Management App

**Estimated Time to Complete:** 4-5 hours.

**Scenario:** A coastal state's government agency has contracted theta. to develop an integrated disaster management application. This app should not only provide alerts during natural disasters like hurricanes but also guide citizens with evacuation plans, shelter locations, and real-time traffic updates. Additionally, the app needs to have a portal for citizens to report emergencies and another for them to mark themselves safe.

**Task:**

1. **Sprint Planning**:
   - Define the MVP for the disaster management application.
   - Create a sprint backlog for the first two sprints.
   - **Constraint**: Each sprint is 3 weeks long. The MVP needs a soft launch before the upcoming hurricane season, which is 4 months away.
2. **Risk Management & Contingency Planning**:
   - Identify potential technical and real-world risks (like heavy app traffic during disasters).
   - Propose both preventive measures and contingency plans.
   - **Constraint**: Focus on the top 5 risks that could severely impede the application's functionality during critical times.
3. **Retrospective & Continuous Learning**:
   - Propose a structure for sprint retrospectives.
   - Detail a plan for post-disaster reviews to learn and iterate for future versions.
   - **Constraint**: Each retrospective is limited to 90 minutes. The project has been allocated only 20 hours of post-launch review time for the entire year.

**Submission Requirements:**
- Compile your sprint backlog, contingency plans, and sprint retro plan into a comprehensive document and submit following the challenge instructions above.

**Evaluation Criteria:**

1. **Sprint Planning**:
   - Clear definition of the MVP considering the impending hurricane season.
   - Logical and feasible sprint backlogs, keeping in mind sprint durations.
   - Ability to align development priorities with real-world urgency.
2. **Risk Management & Contingency Planning**:
   - Identification of significant risks that could impede functionality.
   - Practicality and effectiveness of proposed mitigation strategies.
   - Demonstrated foresight in anticipating challenges and proactive planning.
3. **Retrospective & Continuous Learning**:
   - Structure and effectiveness of the proposed retrospective format.
   - Recognition of the importance of post-disaster reviews.
   - Strategies for iterative improvement based on feedback and learnings.

**Additional Criteria**:
- **Presentation & Structure**: Clear and organized presentation of ideas, logical flow, and coherence.
- **Justification & Rationale**: Ability to provide reasons for decisions, showing depth of thought.
- **Adherence to Constraints**: Respecting the given constraints while showcasing flexibility and creativity.
- **Real-world Relevance**: Demonstrating an understanding of real-world challenges, user needs, and stakeholder interests.
- **Communication Skills**: Clarity in expression, ability to convey complex ideas simply, and attention to detail.

**FAQS:**

**Q:** What will you be grading me on?

**A:** See the above "Evaluation Criteria"

**Q:** Will I have a chance to explain my choices?

**A:** Feel free to comment your code, or put explanations in a pull request within the repo. If we proceed to a phone interview, we’ll be asking questions about why you made the choices you made in the technical interview.

**Q:** Why doesn't the test include XY and Z?

**A:** Dope question! PLEASE free to tell us how to make the test better. Or, you know, fork it and improve it! 
