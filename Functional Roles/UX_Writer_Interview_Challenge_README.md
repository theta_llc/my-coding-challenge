------------------------------- Challenge instructions -------------------------------

Once you complete this exercise, please email the submission to jobs@theta-llc.com with your name, a hyphen, the position you're interviewing for, a colon, and 'Interview Challenge Submission' for in the subject line. (e.g. 'John/Jane Public - UX Writer: Interview Challenge Submission').

Please don't hesitate to contact your interviewer if you have any questions or feedback about the test. We're looking for cleverness and attention to detail, not how quickly you can get it done.

Please use whichever tools you're comfortable with (unless the position you applied to had a specfic tool/technology specification). As a Copywriter you have to be able demonstrate a deep understanding of your target audience's needs and be nuanced in your approach to content creation, so represent yourself with your strongest tools or approaches or choices that demonstrate your methods and approaches. As long as you can provide reasoning on your choices, it's fine with us.

**Project:** OPM.gov Update

**Estimated Time to Complete:** 1-2 hours.

**Tools Needed:** Microsoft Word or Google Docs. 

**Scenario:** theta. is partnering with the Federal Office of Personnel Management to redo OPM’s website, OPM.gov. We are replacing the existing site with content we will create using user-focused and inclusive language that adheres to the Plain Language Act.

**Task:**

Take the existing OPM.gov page that introduces Federal employee Dental & Vision benefits (https://www.opm.gov/healthcare-insurance/dental-vision/) and transform it into content suitable for the new site. Suggest ways to organize the content so the intended users can easily find the information that interests them most. You may want to suggest breaking the content into multiple pages or bringing content from other pages on the site into this one. Include questions you would ask SMEs to understand the content better and address any confusing information. 

Focus on the audience you’re writing for and use a friendly yet professional tone.

**Target Audience**: Federal Employees and Prospective Federal Employees.

**Resources**:
- [18F Content Guide](https://guides.18f.gov/content-guide/our-style/)
- [plainlanguage.gov](https://www.plainlanguage.gov/)
- [AP Style Book](https://www.apstylebook.com/) _(you can get a 14-day free trial if you don’t own the guide)_

**Submission Requirements:**
- Send us a document submission with your proposed content for the new Dental & Vision page (or pages). Include how you would organize the content and rewrite what is on the current page. We are not looking for a publishable page but, instead, close-to-complete content that you would provide to a SME to answer questions and review for accuracy.

**Evaluation Criteria:**

- Is your writing coherent, and is the content accurate? 
- Do you use a tone that is appropriate for the intended audience?
- Do you identify areas that require SME review and ask questions that will elicit responses that clarify confusing information?
- Do you explain complex subjects in an engaging, easy-to-understand way?
- Do you adhere to the style and content guidelines from the Resources above?

**FAQS:**

**Q:** What will you be grading me on?

**A:** See the above "Evaluation Criteria"

**Q:** Will I have a chance to explain my choices?

**A:** Feel free to comment your submission, or put explanations in a pull request within the repo. If we proceed to a phone interview, we’ll be asking questions about why you made the choices you made in the technical interview.

**Q:** Why doesn't the test include XY and Z?

**A:** Dope question! PLEASE free to tell us how to make the test better. Or, you know, fork it and improve it! 
