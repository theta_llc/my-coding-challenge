------------------------------- Challenge instructions -------------------------------

Once you complete this exercise, please email the submission to jobs@theta-llc.com with your name, a hyphen, the position you're interviewing for, a colon, and 'Interview Challenge Submission' for in the subject line. (e.g. 'John/Jane Public - Product Manager: Interview Challenge Submission').

Please don't hesitate to contact your interviewer if you have any questions or feedback about the test. We're looking for cleverness and attention to detail, not how quickly you can get it done.

Please use whichever tools you're comfortable with (unless the position you applied to had a specfic tool/technology specification). As a Product Manager you help identify the customer need and the larger business objectives that a product or feature will fulfill, and are expected to articulate what success looks like for a product, and rally a team to turn that vision into a reality, so represent yourself with your strongest tool choices that demonstrate your methods and approaches. As long as you can provide reasoning on your choices, it's fine with us.

**Project:** City Government Streamlined Web Portal

**Estimated Time to Complete:** 4-5 hours.

**Scenario:** theta. is partnering with a city's government department. The goal is to create a portal that streamlines not only tax filing but also integrates with other city services such as public transportation, event permits, and utility payments. The challenge is the diverse age demographic of the city, from tech-savvy millennials to senior citizens who might be less familiar with digital tools.

**Task:**

1. **MVP Definition**:
   - Define the core features for the MVP considering the diverse user base.
   - **Constraint**: You can select only 5 core features for the MVP. Also, the total development time for the MVP should not exceed 6 weeks.
2. **User Stories and Backlog Creation**:
   - Draft diverse user stories catering to different age groups.
   - Create a prioritized product backlog for the first two sprints.
   - **Constraint**: Your development team for the first two sprints consists of 1 UX designer, 2 frontend developers, 1 backend developer, and 1 database expert.
3. **Segmented Feedback Mechanism**:
   - Propose a strategy for gathering user feedback post each release, segmented by user demographics.
   - **Constraint**: Choose only 2 primary feedback channels but detail how you'd customize them for different age groups.

**Submission Requirements:**
- Compile your backlogs, resource plans, and KPIs into a comprehensive document and submit following the challenge instructions above.

**Evaluation Criteria:**

1. **MVP Definition**:
   - Clarity of thought in defining core features.
   - Justification for the chosen features based on user needs and constraints.
   - Understanding of the diverse user base and their requirements.
2. **User Stories and Backlog Creation**:
   - Quality and comprehensiveness of user stories.
   - Logical prioritization in the product backlog, keeping in mind the team composition.
   - Demonstrated understanding of agile methodologies and sprint planning.
3. **Segmented Feedback Mechanism**:
   - Appropriateness of chosen feedback channels for different age groups.
   - Practicality of feedback collection and integration methods.
   - Recognition of the importance of continuous feedback in agile development.

**Additional Criteria**:
- **Presentation & Structure**: Clear and organized presentation of ideas, logical flow, and coherence.
- **Justification & Rationale**: Ability to provide reasons for decisions, showing depth of thought.
- **Adherence to Constraints**: Respecting the given constraints while showcasing flexibility and creativity.
- **Real-world Relevance**: Demonstrating an understanding of real-world challenges, user needs, and stakeholder interests.
- **Communication Skills**: Clarity in expression, ability to convey complex ideas simply, and attention to detail.

**FAQS:**

**Q:** What will you be grading me on?

**A:** See the above "Evaluation Criteria"

**Q:** Will I have a chance to explain my choices?

**A:** Feel free to comment your code, or put explanations in a pull request within the repo. If we proceed to a phone interview, we’ll be asking questions about why you made the choices you made in the technical interview.

**Q:** Why doesn't the test include XY and Z?

**A:** Dope question! PLEASE free to tell us how to make the test better. Or, you know, fork it and improve it! 
