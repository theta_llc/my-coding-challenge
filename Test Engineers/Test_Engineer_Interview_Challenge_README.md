------------------------------- Challenge instructions -------------------------------

Once you complete this exercise, please email the submission to jobs@theta-llc.com with your name, a hyphen, the position you're interviewing for, a colon, and 'Interview Challenge Submission' for in the subject line. (e.g. 'John/Jane Public - Test Engineer: Interview Challenge Submission').

Please don't hesitate to contact your interviewer if you have any questions or feedback about the test. We're looking for cleverness and attention to detail, not how quickly you can get it done.

Please use whichever language you're comfortable with (unless the position you applied to had a specfic language/technology specification). As a Test Engineer, you're expected to test a plethora of different applications running on different frameworks and written in different languages, so represent yourself with your strongest testing framework and approach choices. As long as you can provide instructions on how to execute it so we can see the results, it's fine with us.

**Estimated Time to Complete:** 3-3.5 hours.

**Assessment Area 1:** Test Case Desgin

**Project:** 
For this project, you are a test engineer on a team creating a gift wrapping option feature for an e-commerce platform. You are to design comprehensive test cases for the new feature.

**Description:** 
Users should have the option to select "Gift Wrapping" during checkout. If selected, users can also add a personalized message that will be printed on a card.

User Flow:

1. User navigates to the checkout page.
2. User sees a checkbox for 'Gift Wrapping'.
3. Upon selecting the checkbox, a text box appears for a personalized message.
4. User can type in a personalized message.
5. User finalizes the checkout.

Constraints:

- The personalized message should not exceed 150 characters.
- Only alphanumeric characters and basic punctuation (.,!?'") are allowed in the message.
- The gift wrapping option incurs an additional fee.

**Tasks:**
1. Design test cases to validate the successful implementation of the feature.
2. Consider positive, negative, boundary, and edge cases.
3. Document any assumptions you make about the feature's expected behavior.

**Mock Data:**
(Note: This is just a brief description; the focus is on the test design, not the data.)

{
    "user": {
        "id": 1234,
        "name": "John Doe",
        "cart_items": [
            {"item_id": 5678, "name": "Watch", "price": 199.99}
        ],
        "gift_wrap_selected": true,
        "personalized_message": "Happy Birthday, Jane!"
    }
}

**Submission Requirements:**
1. Document your test cases: Use a format that's clear and easy to understand. You can use tools like Excel, Google Sheets, or any test case management tool if you're familiar with them. If not, a simple word document should suffic. Submit following the challenge instructions above.

**Assessment Area 2:** Bug Reporting

**Project:** 
For this project, you will be provided with a basic web-based calculator app. Identify and report any bugs or issues you find.

**Description:** 
The calculator can perform basic arithmetic operations: addition, subtraction, multiplication, and division. It can handle decimals, and users should be able to clear the entry or reset the calculator.

Allowed Operations:

- Addition
- Subtraction
- Multiplication
- Division
- Square Root
- Square
- Percentage

Expected Inputs:

1. 12 + 8 should equal 20.
2. 50 - 25 should equal 25.
3. 7 ÷ 0 should produce an error "Error: Division by zero."
4. 10.5 × 3 should equal 31.5.
5. Square root of 144 should equal 12.
6. 16 squared should equal 256.
7. 250 as a percentage should equal 2.5.
8. 0 + 100 should equal 100.
9. 45 ÷ 5 should equal 9.
10. 9 × 3 should equal 27.
11. 55 as a percentage should equal 2.75.
12. Square root of 81 should equal 9.
13. 0 ÷ 0 should produce an error "Error: Indeterminate form."

**Tasks:**
1. Test the calculator for various input scenarios above.
2. Document any bugs or issues you identify. Each report should include:
    - Bug title
    - Steps to reproduce
    - Expected result
    - Actual result
    - Severity (e.g., Critical, High, Medium, Low)
    - Any additional notes or screenshots

**Calculator App:**
Link to the Calculator app is here: https://651355de2d6dfc1e4398c581--astounding-creponne-64cb51.netlify.app/

**Submission Requirements:**
1. Document your bug report and submit following the challenge instructions above.

**Assessment Area 3:** Automation

**Project:** 
For this project, you will write automated tests for a RESTful API of a task management system.

**Description:** 
The API for the task management system has endpoints for CRUD operations on tasks. Each task has a title, description, due date, and status.

**Tasks:**
1. Set up a testing framework of your choice (e.g., Postman, JUnit, TestNG).
2. Write automated tests for each endpoint: Create, Read, Update, and Delete.
3. Ensure your tests cover various scenarios, including success cases, error cases, and edge cases.
4. Provide documentation on how to run the tests and any setup required. Keep a record of all the test cases you're automating, their expected results, and actual outcomes. Include any assumptions or clarifications you've made.

**Mock Data:**
(Note: This is just a brief description; the focus is on the test design, not the data.)

{
    "tasks": [
        {
            "id": 1,
            "title": "Complete project proposal",
            "description": "Draft and finalize the project proposal for the upcoming client meeting.",
            "due_date": "2023-10-05",
            "status": "Pending"
        },
        {
            "id": 2,
            "title": "Review codebase",
            "description": "Review the recent commits to ensure code quality and standards.",
            "due_date": "2023-09-28",
            "status": "Completed"
        }
    ]
}

API Description: RESTful API for a task management system

Endpoints:

1. Create Task:
    - URL: /tasks
    - Method: POST
    - Request Body:
        - Title (string)
        - Description (string)
        - Due Date (format: yyyy-mm-dd)
        - Status (values: Pending or Completed)
    - Response: Task ID

2. Read Task:
    - URL: /tasks/{task_id}
    - Method: GET
    - Response:
        - ID (integer)
        - Title (string)
        - Description (string)
        - Due Date (format: yyyy-mm-dd)
        - Status (values: Pending or Completed)

3. Update Task:
    - URL: /tasks/{task_id}
    - Method: PUT
    - Request Body (all fields optional):
        - Title (string)
        - Description (string)
        - Due Date (format: yyyy-mm-dd)
        - Status (values: Pending or Completed)
    - Response: Success or Error message

4. Delete Task:
    - URL: /tasks/{task_id}
    - Method: DELETE
    - Response: Success or Error message

Sample Tasks:

1. Task 1:
    - ID: 1
    - Title: Complete project proposal
    - Description: Draft and finalize the project proposal for the upcoming client meeting.
    - Due Date: 2023-10-05
    - Status: Pending

2. Task 2:
    - ID: 2
    - Title: Review codebase
    - Description: Review the recent commits to ensure code quality and standards.
    - Due Date: 2023-09-28
    - Status: Completed

**Submission Requirements:**
1. Once you've automated the test cases, submit your automation scripts, results, and documentation following the challenge instructions above.

**Evaluation Criteria:**
1. Technical Proficiency (e.g., script quality, bug identification, test structure and clarity)
2. Problem-Solving and Critical Thinking (e.g., scenario handling and edge case identifications)
3. Attention to Detail (e.g., accuracy & observation skills)
4. Documentation (e.g., soft communication skills, clarity, organization, and completeness )
5. Overall Approach (e.g., methodology used, proactiveness)

**FAQS:**

**Q:** What will you be grading me on?

**A:** See the above "Evaluation Criteria"

**Q:** Will I have a chance to explain my choices?

**A:** Feel free to comment your code, or put explanations in a pull request within the repo. If we proceed to a phone interview, we’ll be asking questions about why you made the choices you made in the technical interview.

**Q:** Why doesn't the test include XY and Z?

**A:** Dope question! PLEASE free to tell us how to make the test better. Or, you know, fork it and improve it! 
