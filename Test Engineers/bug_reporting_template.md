Bug Reporting Template:

---

Bug ID: [Unique identifier or number for tracking]

Title: [Brief, descriptive title for the bug]

Date Reported: [Date when the bug was identified]

Reported By: [Name of the tester]

Environment: 
- Device: [e.g., Desktop, iPhone X, Samsung Galaxy S10]
- OS: [e.g., Windows 10, iOS 14, Android 11]
- Browser/Version (if applicable): [e.g., Chrome 91, Safari 14]

Priority: [e.g., Critical, High, Medium, Low]

Severity: [e.g., Blocker, Major, Minor, Trivial]

Description:
A detailed description of the bug, including the overall impact on functionality.

Steps to Reproduce:
1. 
2. 
3. 
... [Step-by-step process to reproduce the bug]

Expected Result:
What should happen or what was the intended behavior.

Actual Result:
What is currently happening, including any error messages if applicable.

Attachments:
Screenshots, logs, or any other relevant files to give more context about the bug.

Notes:
Any additional information or observations about the bug.

---

Fill out the template with the relevant details, ensuring clarity and detail for effective communication.
